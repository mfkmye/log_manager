function NavObj() {
    var debug = false,
        _self = this,
        moveStart,
        limitWidth = window.innerWidth / 4,
        nav = $('.nav'),
        canTouchMove = false,
        listData = null;

    this.constructor = function () {
        if (debug) console.log('constructor');
        canTouchMove = false;
        this.createListView();
        this.bindListEvent();
        this.bindScrollShowNav.call(this);
    }
    /**
     * 为nav绑定所有结构事件
     */
    this.bindScrollShowNav = function () {
        $(document).on($.isPC() ? 'mousedown' : 'touchstart', function (e) {
            moveStart = e.clientX || e.touches[0].clientX;
            if (debug) console.log(e, moveStart);
            if (moveStart < limitWidth) {
                $(document).on($.isPC() ? 'mousemove' : 'touchmove', _self.bindMoveContinue);
                $(document).on($.isPC() ? 'mouseup' : 'touchend', _self.bindMoveEnd);
            }
        });
        if ($.isPC()) {
            $(window).on('resize', function (e) {
                _self.toggleNav(window.innerWidth > 768);
            });
        }
        $('.nav').on('click', function (e) {
            console.log('click ', e);
            _self.toggleNav(false);
        });
    }
    /**
     * nav block show or hide
     */
    this.toggleNav = function () {
        var flag = arguments[0];
        if (flag == undefined) {
            $('.nav').toggle();
        } else if (flag == true) {
            $('.nav').show();
        } else if (flag == false) {
            $('.nav').hide();
        }
    };

    /**
     * create nav list
     * data: [{id: Number, name: String}]
     */
    this.createListView = function (data) {
        _self.setRowSpanText();
    }

    /**
     * set nav list data, if data is empty clean all;
     * data: [{id: Number, name: String}]
     */
    this.setListData = function (data) {
        if ($.isEmptyObject(data)) {
            listData = null;
        }
        listData = data;
        this.createListView(data);
    }
    this.bindMoveContinue = function (e) {
        var x = e.clientX || e.touches[0].clientX,
            moveD = x - moveStart;
        if (moveD > limitWidth) {
            if (debug) console.log('bindMoveContinue : ', moveD, limitWidth);
            _self.toggleNav(true);
        }
    }
    this.bindMoveEnd = function () {
        if (debug) console.log('bindMoveEnd');
        $(document).off($.isPC() ? 'mousemove' : 'touchmove', _self.bindMoveContinue);
        $(document).off($.isPC() ? 'mouseup' : 'touchend', _self.bindMoveEnd);
    }
    this.setRowSpanText = function () {
        $('.open').text("∨");
        $('.shrink').text("∧");
    }
    this.bindListEvent = function () {
        if (debug) console.log('bindListEvent');
        $('.nav_content').on('click', 'li', function (e) {
            if (debug) console.log('bindListEvent click', e, this, $(e).data());
            if ($(this).attr('data-key')) {
                var target = $(this).find('.open');
                if (target.length > 0) {
                    target.removeClass('open').addClass('shrink');
                } else {
                    target = $(this).find('.shrink');
                    target.removeClass('shrink').addClass('open');
                }
                $(this).parent().find('[data-value]').toggle();
                _self.setRowSpanText();
            } else {
                $('.nav_content li').removeClass('active');
                $(this).addClass('active');
                if (window.innerWidth <= 768) {
                    _self.toggleNav(false);
                }
            }
            return false;
        })
    }
    this.constructor();
}